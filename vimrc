"VIM Configuration - Vincent Jousse
" Annule la compatibilite avec l’ancetre Vi : totalement indispensable
set nocompatible
"-- Clavier bépo
source ~/.vim/vimrc.bepo
source ~/.vim/nerdtree.bepo
" -- Affichage
"Affiche le numero des lignes
set title
set number
set relativenumber
set ruler
set wrap
set scrolloff=3
set list
set listchars=nbsp:°
set mouse=a

" -- Recherche
set ignorecase
set smartcase

" -- Netwr
" Tweaks for browsing
let g:netrw_banner=0        " disable annoying banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'

" -- Surligner recherche
set hlsearch 

" -- Empeche Vim de beeper
set noerrorbells 

" -- Active le comportement habituel de la touche retour en arriere
set backspace=indent,eol,start

" -- Cache les fichiers lors de l’ouverture d’autres fichiers
set hidden

" -- Active la coloration syntaxique
syntax enable

" -- Active les comportements specifiques aux types de fichiers comme
" -- la syntaxe et l’indentation
filetype on
filetype plugin on
filetype indent off

" -- Utilise la version sombre de Solarized
set background=light

" -- mapleader
let mapleader = ","
let maplocalleader =","

" -- Tab = 4spaces
set tabstop=2
set expandtab
set shiftwidth=2

" -- Longeur ligne 79
set textwidth=78

" -- Indentation intelligent
set smartindent

" -- Conserve l’indentation sur une nouvelle ligne
set autoindent 

" -- File explorer
set wildignorecase

set wildmenu
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
  " let Vundle manage Vundle, required
  Plugin 'VundleVim/Vundle.vim'
  " Indent
  Plugin 'Yggdroot/indentLine.git'
  " Yaml syntax
"  Plugin 'stephpy/vim-yaml'
  "Fold yaml
  Plugin 'pedrohdz/vim-yaml-folds'
  " Show marks
  Plugin 'kshenoy/vim-signature'
  " ALE
  Plugin 'dense-analysis/ale'
  " Fugitive Git plugin
  Plugin 'tpope/vim-fugitive'
  " Vim notes
  Plugin 'xolox/vim-notes'
  Plugin 'xolox/vim-misc'
  " Vim varnish
  Plugin 'fgsch/vim-varnish'
  "  Theme
  Plugin 'altercation/vim-colors-solarized'
  "Vim airline
  Plugin 'bling/vim-airline'
  " Theme
  Plugin 'sainnhe/sonokai'
  " FZF
  Plugin 'junegunn/fzf', { 'do': { -> fzf#install() } }
  " Vim grepper
  Plugin 'mhinz/vim-grepper'
  "jinja syntax"
  Plugin 'glench/vim-jinja2-syntax'
  Plugin 'dhruvasagar/vim-table-mode'

  " Quickfix slpit
  Plugin 'yssl/QFEnter'
call vundle#end()            " required

filetype plugin indent on
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" -- Ale Config
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_sign_error = '✘'
let g:ale_sign_warning = '⚠'
let g:ale_lint_on_text_changed = 'never'

" -- Vim notes config
let g:notes_directories = ['~/Documents/Notes']
let g:notes_suffix = '.md'

" -- Vim varnish
let g:vcl_fold = 2
autocmd BufRead,BufNewFile *.vcl setfiletype vcl

let g:python_host_prog  = '/usr/bin/python2.7'
let g:python3_host_prog  = '/usr/bin/python3'
let g:ycm_path_to_python_interpreter = '/usr/bin/python3'


"Find path
set path+=$PWD/**

" find files and populate the quickfix list
fun! FindFiles(filename)
  let error_file = tempname()
  silent exe '!find . -name "'.a:filename.'" | xargs file | sed "s/:/:1:/" > '.error_file
  set errorformat=%f:%l:%m
  exe "cfile ". error_file
  copen
  call delete(error_file)
endfun
command! -nargs=1 FindFile call FindFiles(<q-args>)

colorscheme sonokai

" keybind to open vim and reload config
nnoremap <leader>emv :vsp $MYVIMRC<CR>
nnoremap <leader>umv :source $MYVIMRC<CR>
"FZF
nnoremap <leader>f :FZF ~/depots/<CR>

"Grepper
let g:grepper               = {}
let g:grepper.tools         = ['rg', 'git', 'grep']
au BufNewFile,BufRead *.html,*.htm,*.shtml,*.stm set ft=jinja

" This is only availale in the quickfix window, owing to the filetype
" restriction on the autocmd (see below).
function! <SID>OpenQuickfix(new_split_cmd)
  " 1. the current line is the result idx as we are in the quickfix
  let l:qf_idx = line('.')
  " 2. jump to the previous window
  wincmd p
  " 3. switch to a new split (the new_split_cmd will be 'vnew' or 'split')
  execute a:new_split_cmd
  " 4. open the 'current' item of the quickfix list in the newly created buffer
  "    (the current means, the one focused before switching to the new buffer)
  execute l:qf_idx . 'cc'
endfunction

autocmd FileType qf nnoremap <buffer> <C-v> :call <SID>OpenQuickfix("vnew")<CR>
autocmd FileType qf nnoremap <buffer> <C-h> :call <SID>OpenQuickfix("split")<CR>

nnoremap <leader>aide :vsp ~/.vim/README.md<CR>
nnoremap <leader>? :vsp ~/.vim/README.md<CR>
